using System.Reflection;

namespace Ecommerce.Models;

public class Roles
{
    public const string Admin = "Admin";
    public const string Customer = "Customer";

    public static List<string> GetRoles()
    {
        return typeof(Roles)
            .GetFields(BindingFlags.Public | BindingFlags.Static)
            .Select(field => field.Name)
            .ToList();
    }
}
