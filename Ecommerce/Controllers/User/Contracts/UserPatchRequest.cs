namespace Ecommerce.Controllers.Contracts;

public class UserPatchRequest
{
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public string? PhoneNumber { get; set; }
    public string? OldPassword { get; set; }
    public string? NewPassword { get; set; }
    public string? DefaultShippingAddress { get; set; }
    public string? BillingAddress { get; set; }
    

}

