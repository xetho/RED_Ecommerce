using Ecommerce.Data;
using Ecommerce.Models;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Configuration;

public static class DependencyInjection
{
    public static IServiceProvider RunMigrations(this IServiceProvider serviceProvider)
    {
        using (var scope = serviceProvider.CreateScope())
        {
            var services = scope.ServiceProvider;
            var context = services.GetRequiredService<ApplicationDbContext>();

            if (context.Database.IsRelational() && context.Database.GetPendingMigrations().Any())
            {
                // GetPendingMigrations throws an error if the db isn't relational. For testing using in-memory db for instance.
                // app.Logger.LogWarning("Found pending Db migrations on the database at: {}.", context.Database.GetConnectionString());
                // app.Logger.LogInformation("Attempting to apply pending migrations...");

                context.Database.EnsureCreated();
            }
            else
            {
                // app.Logger.LogInformation("Found no pending migrations. Proceeding...");
            }
        }

        return serviceProvider;
    }
}