using System.Security.Claims;
using Ecommerce.Controllers;
using Ecommerce.Controllers.Contracts;
using Ecommerce.Models;
using Ecommerce.Services;
using Ecommerce.Services.Interfaces;
using Ecommerce.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Logging;
using Moq;

namespace Ecommerce.Tests.Unit.Authentication.Controller;

public class AuthenticationControllerTest
{
    private AuthController _authController;
    private readonly Mock<IAuthService> _mockAuthService;
    private readonly Mock<IUserAccountService> _mockUserAccountService;
    private readonly Mock<ILogger<AuthController>> _mockLogger;


    public AuthenticationControllerTest()
    {
        _mockAuthService = new Mock<IAuthService>();
        _mockUserAccountService = new Mock<IUserAccountService>();
        _mockLogger = new Mock<ILogger<AuthController>>();

        _authController = new AuthController(
            _mockAuthService.Object,
            _mockUserAccountService.Object,
            _mockLogger.Object);
    }

    [Fact]
    public async Task SampleTest()
    {
        //arrange
        _mockUserAccountService.Setup(
            service => service.GetAllUsers()
        ).ReturnsAsync(new List<UserDto>()
        {
            new UserDto(new User("email@email.com", "firstname", "lastname"), "access_token", "refresh_token", "Customer"),
            new UserDto(new User("email2@email.com", "firstname2", "lastnam2"), "access_token", "refresh_token", "Customer"),
            new UserDto(new User("email3@email.com", "firstname3", "lastnam3"), "access_token", "refresh_token", "Customer")
        });

        //act
        var result = await _authController.GetUsers();

        //assert
        var okResult = Assert.IsType<OkObjectResult>(result);
        var list = Assert.IsType<List<UserDto>>(okResult.Value);
        Assert.Equal(3, list.Count);
        Assert.Contains(
            list,
            user => user.Email == "email@email.com" && user.FirstName == "firstname");
        Assert.DoesNotContain(
            list, user => user.Email == "email@email.com" && user.FirstName == "firstname2");
    }

    [Fact]
    public async Task CustomerCreate_ReturnsCreatedResult_OnSuccess()
    {
        // Arrange
        var request = new RegistrationRequest(
            firstName: "Test",
            lastName: "User",
            email: "testuser@email.com",
            password: "testpassword",
            confirmPassword: "testpassword",
            phoneNumber: "123456774"
        );

        var expectedUser = request.ToUser();

        var expectedUserDto = new UserDto(
            user: expectedUser,
            accessToken: "access token",
            refreshToken: "refresh token",
            role: "Customer"
        )
        {
            PhoneNumber = "123456774",
        };

        _mockAuthService
            .Setup(
                service => service.RegisterCustomer(
                    It.Is<User>(u=>u.Email == expectedUser.Email),
                     request.Password))
            .ReturnsAsync(ServiceResponse<UserDto>.SuccessResponse(
                statusCode: 201,
                data: expectedUserDto));


        _mockAuthService
            .Setup(
                service => service.SendConfirmationEmail(
                    It.Is<User>(u => u.Email == expectedUser.Email),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(), 
                    It.IsAny<string>())
            )
            .ReturnsAsync(ServiceResponse<bool>.SuccessResponse(
                statusCode: 200,
                data: true));


        var mockHttpContext = new Mock<HttpContext>();
        var mockHttpRequest = new Mock<HttpRequest>();

        mockHttpRequest.Setup(r => r.Host).Returns(new HostString("localhost"));
        mockHttpRequest.Setup(r => r.PathBase).Returns(new PathString("/auth"));
        mockHttpContext.Setup(c => c.Request).Returns(mockHttpRequest.Object);

        _authController.ControllerContext = new ControllerContext
        {
            HttpContext = mockHttpContext.Object
        };

        // Mock IUrlHelperFactory and IActionContextAccessor
        var mockUrlHelper = new Mock<IUrlHelper>(MockBehavior.Strict);
        mockUrlHelper
            .Setup(
                x => x.Action(
                    It.IsAny<UrlActionContext>()
                )
            )
            .Returns("mockedUrl")
            .Verifiable();

        // Inject the mocks into the controller
        _authController.Url = mockUrlHelper.Object;

        // Act
        var result = await _authController.Register(request);
        // Assert
        var createdResult = Assert.IsType<ObjectResult>(result);
        var returnedUser = Assert.IsType<UserDto>(createdResult.Value);
        Assert.Equal(expectedUserDto, returnedUser);
    }

    [Fact]
    public async Task CustomerLogin_ReturnsUserDto_OnSuccess()
    {
        // Arrange
        var testEmail = "test@email.email";
        var testPassword = "testpassword";
        var loginRequest = new LoginRequest(
            Email: testEmail,
            Password: testPassword
        );

        var expectedUser = new UserDto(
            firstName: "some name",
            lastName: "some name",
            email: testEmail
        );

        _mockAuthService.Setup(
            service => service.LoginUser(loginRequest.Email, loginRequest.Password)
        ).ReturnsAsync(
            ServiceResponse<UserDto>.SuccessResponse(
                statusCode: 200,
                data: expectedUser
            )
        );

        // Act
        var result = await _authController.Login(loginRequest);

        // Assert
        var okResult = Assert.IsType<OkObjectResult>(result);
        var returnedUser = Assert.IsType<UserDto>(okResult.Value);
        Assert.Equal(returnedUser, expectedUser);
    }

    [Fact]
    public async Task CustomerLogin_ReturnsException_OnIncorrectPassword()
    {
        // Arrange
        const string testEmail = "test@email.email";
        const string testPassword = "testpassword";
        var loginRequest = new LoginRequest(testEmail, testPassword);

        _mockAuthService.Setup(
            service => service.LoginUser(loginRequest.Email, loginRequest.Password)
        ).ReturnsAsync(
            ServiceResponse<UserDto>.FailResponse(
                statusCode: StatusCodes.Status401Unauthorized,
                errorDescription: ""
            )
        );

        // Act
        var result = await _authController.Login(loginRequest);

        // Assert
        var httpResponse = Assert.IsType<ObjectResult>(result);
        Assert.Equal(httpResponse.StatusCode, StatusCodes.Status401Unauthorized);
    }


    [Fact]
    public async Task Login_ReturnsOkResult_WithUserAndTokens()
    {
        // Arrange
        var loginRequest = new LoginRequest(
            Email: "testuser@email.com",
            Password: "testpassword");

        var expectedUser = new UserDto
        {
            Id = Guid.NewGuid(),
            FirstName = "Test",
            LastName = "User",
            Email = "testuser@email.com",
            DefaultShippingAddress = "123 test st",
            BillingAddress = "456 test st",
            AccessToken = "access token",
            RefreshToken = "refresh token",
            Role = "customer"
        };

        _mockAuthService
            .Setup(service => service.LoginUser(loginRequest.Email, loginRequest.Password))
            .ReturnsAsync(
                ServiceResponse<UserDto>.SuccessResponse(
                    statusCode: StatusCodes.Status200OK, data: expectedUser));

        // Act
        var result = await _authController.Login(loginRequest);

        // Assert
        var okResult = Assert.IsType<OkObjectResult>(result);
        var user = Assert.IsType<UserDto>(okResult.Value);
        Assert.Equal(expectedUser, user);
    }

    [Fact]
    public async Task Logout_ReturnsNoContentResult__WithValidUser()
    {
        // Arrange
        var accessToken = "access token";

        var testUserId = Guid.NewGuid();
        var mockHttpContext = new Mock<HttpContext>();
        var mockHttpRequest = new Mock<HttpRequest>();

        mockHttpRequest.Setup(r => r.Host).Returns(new HostString("localhost"));
        mockHttpRequest.Setup(r => r.PathBase).Returns(new PathString("/auth"));
        mockHttpRequest.Setup(r => r.Headers).Returns(
            new HeaderDictionary
            {
                {
                    "Authorization", $"Bearer {accessToken}"
                }
            }
        );

        mockHttpContext.Setup(c => c.Response).Returns(new Mock<HttpResponse>().Object);
        mockHttpContext.Setup(c => c.User).Returns(new ClaimsPrincipal());
        mockHttpContext.Setup(c => c.Items).Returns(
            new Dictionary<object, object?>
            {
                {"UserId", testUserId}
            }

        );
        mockHttpContext.Setup(c => c.Request).Returns(mockHttpRequest.Object);
        // mockHttpContext.Setup(r => r.Items.TryGetValue("UserId")).Returns(testUserId);

        _authController.ControllerContext = new ControllerContext
        {
            HttpContext = mockHttpContext.Object
        };

        // Act
        var result = await _authController.LogOut();

        // Assert
        Assert.IsType<NoContentResult>(result);
    }

    [Fact]
    public async Task Refresh_ReturnsNewTokens_OnValidRequest()
    {
        // Arrange
        var expiredToken = "expired token";
        var refreshToken = "refresh token";

        var request = new TokenRequestModel(
            AccessToken: expiredToken,
            RefreshToken: refreshToken
        );

        var expectedUser = new UserDto
        {
            Id = Guid.NewGuid(),
            FirstName = "Test",
            LastName = "User",
            Email = "testuser@email.com",
            DefaultShippingAddress = "123 test st",
            BillingAddress = "456 test st",
            AccessToken = "new access token",
            RefreshToken = "new refresh token"
        };

        _mockAuthService.Setup(service => service.RefreshToken(expiredToken, refreshToken))
            .ReturnsAsync(
                ServiceResponse<UserDto>.SuccessResponse(
                    statusCode: StatusCodes.Status200OK,
                    data: expectedUser
                )
            );

        // Act
        var result = await _authController.Refresh(request);

        // Assert
        var response = Assert.IsType<OkObjectResult>(result);
        var user = Assert.IsType<UserDto>(response.Value);
        Assert.Equal(user, expectedUser);
    }

    [Fact]
    public async Task Refresh_ReturnsException_OnInValidRequest()
    {
        // Arrange
        var expiredToken = "expired token";
        var refreshToken = "refresh token";

        var request = new TokenRequestModel(
            AccessToken: expiredToken,
            RefreshToken: refreshToken
        );

        var expectedUser = new UserDto
        {
            Id = Guid.NewGuid(),
            FirstName = "Test",
            LastName = "User",
            Email = "testuser@email.com",
            DefaultShippingAddress = "123 test st",
            BillingAddress = "456 test st",
            AccessToken = "new access token",
            RefreshToken = "new refresh token"
        };

        _mockAuthService.Setup(service => service.RefreshToken(expiredToken, refreshToken))
            .ReturnsAsync(
                ServiceResponse<UserDto>.FailResponse(
                    statusCode: StatusCodes.Status401Unauthorized,
                    errorDescription: "Invalid Token"
                )
            );

        // Act
        var result = await _authController.Refresh(request);

        // Assert
        var response = Assert.IsType<UnauthorizedObjectResult>(result);
        var error = Assert.IsType<string>(response.Value);
    }
}