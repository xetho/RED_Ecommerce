using System.Security.Claims;
using Ecommerce.Controllers.Contracts;
using Ecommerce.Infrastructure.Authentication;
using Ecommerce.Models;
using Ecommerce.Services;
using Ecommerce.Services.Authentication;
using Ecommerce.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;

namespace Ecommerce.Tests.Unit;

public class AuthServiceTest
{
    private AuthService _authService;
    private readonly Mock<IUserAccountService> _mockUserAccountService;
    private readonly Mock<UserManager<User>> _mockUserManager;
    private readonly IJwtTokenGenerator _mockJwtTokenGenerator;
    private readonly Mock<IEmailService> _mockEmailService;
    private readonly Mock<ILogger<AuthService>> _mockLogger;
    private readonly Mock<SignInManager<User>> _mockSignInManager;
    private readonly List<User> users = new List<User>
    {
        new User("valid_email@example.com", "firstname", "lastname")
        {
            Id = Guid.NewGuid(),
            EmailConfirmed = true,
        },
        new User("valid_unconfirmed@email.com", "firstname2", "lastnam2")
        {
            Id = Guid.NewGuid(),
            EmailConfirmed = false,
        },
        new User("email3@email.com", "firstname3", "lastnam3")
        {
            Id = Guid.NewGuid(),
            EmailConfirmed = false,
        }
    };

    public AuthServiceTest()
    {
        _mockUserAccountService = new Mock<IUserAccountService>();
        _mockEmailService = new Mock<IEmailService>();
        _mockLogger = new Mock<ILogger<AuthService>>();

        // instead of mocking the token generator, we're going to instantiate it with
        // mock values and use it that way instead.
        var jwtSettings = new JwtSettings()
        {
            Secret = "the-super-secret-and-super-duper-long-test-key",
            Issuer = "issuer",
            ExpiryInMinutes = 60,
            Audience = "audience"

        };
        IOptions<JwtSettings> jwtConfig = Options.Create<JwtSettings>(jwtSettings);
        _mockJwtTokenGenerator = new JwtTokenGenerator(jwtConfig);

        _mockUserManager = MockUserManager<User>(users);

        _mockSignInManager = new Mock<SignInManager<User>>(
            _mockUserManager.Object,
            new Mock<IHttpContextAccessor>().Object,
            new Mock<IUserClaimsPrincipalFactory<User>>().Object,
            new Mock<IOptions<IdentityOptions>>().Object,
            new Mock<ILogger<SignInManager<User>>>().Object,
            new Mock<IAuthenticationSchemeProvider>().Object,
            new Mock<IUserConfirmation<User>>().Object
        );


        _authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object
        );
    }

    public static Mock<UserManager<TUser>> MockUserManager<TUser>(
        List<TUser> list) where TUser : class
    {
        var store = new Mock<IUserStore<TUser>>();
        // var identityOptions = new Mock<IOptions<IdentityOptions>>();
        // var passwordHasher = new Mock<IPasswordHasher<TUser>>();
        // var userValidators = new List<IUserValidator<TUser>>();
        // var passwordValidators = new List<IPasswordValidator<TUser>>();
        // var lookupNormalizer = new Mock<ILookupNormalizer>();
        // var errors = new Mock<IdentityErrorDescriber>();
        // var services = new Mock<IServiceProvider>();
        // var logger = new Mock<ILogger<UserManager<TUser>>>();

        Mock<UserManager<TUser>> mockUserManager = new Mock<UserManager<TUser>>(
            store.Object, null, null, null, null, null, null, null, null);

        mockUserManager.Object.UserValidators.Add(new UserValidator<TUser>());
        mockUserManager.Object.PasswordValidators.Add(new PasswordValidator<TUser>());

        mockUserManager.Setup(x => x.DeleteAsync(It.IsAny<TUser>())).ReturnsAsync(
            IdentityResult.Success);
        mockUserManager.Setup(x => x.CreateAsync(It.IsAny<TUser>(), It.IsAny<string>()))
            .ReturnsAsync(IdentityResult.Success);
        mockUserManager.Setup(x => x.UpdateAsync(It.IsAny<TUser>())).ReturnsAsync(
            IdentityResult.Success);
        mockUserManager.Setup(x => x.Users).Returns(
            list.AsQueryable()
        );
        return mockUserManager;
    }

    // public static Mock<SignInManager<KernelToUserDriveMapping>> MockSignInManager<TUser>()
    // {
    //     new Mock
    // }

    // [Fact]
    // public void GetUsers_ReturnsListOfUsers()
    // {
    //     // Act
    //     var result = _authService.GetUsers();

    //     // Assert
    //     foreach (var user in users)
    //     {
    //         Assert.Contains(result, u => u.Email == user.Email);
    //     }
    // }

    [Fact]
    public async Task LoginUser_ValidCredentials_ReturnsSuccessResponse()
    {
        // Arrange

        // setup the request(arguement given to the service)
        var validLoginRequest = new LoginRequest(
            Email: users[0].Email!,
            Password: users[0].PasswordHash!);

        // The user that is trying to login. Should be returned to us at the end
        var expectedUser = users[0];

        // Setup the mocks
        _mockUserManager.Setup(
            userManager => userManager.FindByEmailAsync(
                validLoginRequest.Email)
        ).ReturnsAsync(expectedUser);

        _mockUserManager.Setup(
            userManager => userManager.CheckPasswordAsync(
                expectedUser, "valid_password")
        ).ReturnsAsync(true);

        _mockUserManager.Setup(
            userManager => userManager.GetRolesAsync(expectedUser)
        ).ReturnsAsync(
            new List<string> { "Customer" });

        _mockSignInManager.Setup(
            signInManager => signInManager.PasswordSignInAsync(
                validLoginRequest.Email,
                validLoginRequest.Password,
                It.IsAny<bool>(),
                It.IsAny<bool>())
        ).ReturnsAsync(SignInResult.Success);

        _mockSignInManager.Setup(
            signInManager => signInManager.CreateUserPrincipalAsync(It.IsAny<User>())
        ).ReturnsAsync(new ClaimsPrincipal());

        // Create a new instance of the service with the mocks we set up.
        var authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object
        );

        // Act
        var response = await authService.LoginUser(validLoginRequest.Email, validLoginRequest.Password);

        // Assert
        Assert.True(response.IsSuccess);
        Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
    }

    [Fact]
    public async Task LoginUser_UserNotFound_ReturnsNotFoundResponse()
    {
        // Arrange
        var invalidLoginRequest = new LoginRequest(
            Email: "invalid_email@example.com",
            Password: "some_password");

        User? expectedUser = null;

        _mockUserManager.Setup(
            userManager => userManager.FindByEmailAsync(
                invalidLoginRequest.Email
            )
        ).ReturnsAsync(
            expectedUser);

        _mockSignInManager.Setup(
            signInManager => signInManager.PasswordSignInAsync(
                It.IsNotIn<string>(users.Select(u => u.Email)!),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>())
        ).ReturnsAsync(SignInResult.Failed);

        _mockSignInManager.Setup(
            signInManager => signInManager.CreateUserPrincipalAsync(It.IsAny<User>())
        ).ReturnsAsync(new ClaimsPrincipal());

        var authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object
        );

        // Act
        var response = await authService.LoginUser(invalidLoginRequest.Email, invalidLoginRequest.Password);

        // Assert
        Assert.False(response.IsSuccess);
        Assert.Equal(StatusCodes.Status404NotFound, response.StatusCode);
    }

    [Fact]
    public async Task LoginUser_IncorrectPassword_ReturnsUnauthorizedResponse()
    {
        // Arrange
        var validLoginRequest = new LoginRequest(
            Email: "valid_email@example.com",
            Password: "invalid_password");

        var expectedUser = users[0];

        _mockUserManager.Setup(
            userManager => userManager.FindByEmailAsync(
                validLoginRequest.Email)
        ).ReturnsAsync(expectedUser);

        _mockUserManager.Setup(
            userManager => userManager.CheckPasswordAsync(
                expectedUser, validLoginRequest.Password)
        ).ReturnsAsync(false);

        _mockUserManager.Setup(
            userManager => userManager.GetRolesAsync(expectedUser)
        ).ReturnsAsync(
            new List<string> { "Customer" });

        _mockSignInManager.Setup(
            signInManager => signInManager.PasswordSignInAsync(
                It.IsIn<string>(users.Select(u => u.Email)!),
                It.IsNotIn<string>(new List<string> { expectedUser.PasswordHash! }.Concat(users.Select(u => u.PasswordHash))!),
                It.IsAny<bool>(),
                It.IsAny<bool>())
        ).ReturnsAsync(SignInResult.Failed);

        _mockSignInManager.Setup(
            signInManager => signInManager.CreateUserPrincipalAsync(It.IsAny<User>())
        ).ReturnsAsync(new ClaimsPrincipal());

        var authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object
        );

        // Act
        var response = await authService.LoginUser(validLoginRequest.Email, validLoginRequest.Password);

        // Assert
        Assert.False(response.IsSuccess);
        Assert.Equal(StatusCodes.Status401Unauthorized, response.StatusCode);
    }

    [Fact]
    public async Task LoginUser_UnconfirmedEmail_ReturnsUnauthorizedResponse()
    {
        // Arrange
        var validLoginRequest = new LoginRequest(
            Email: "valid_email@example.com",
            Password: "invalid_password");

        var expectedUser = users[0];
        expectedUser.EmailConfirmed = false;

        _mockUserManager.Setup(
            userManager => userManager.FindByEmailAsync(
                validLoginRequest.Email)
        ).ReturnsAsync(expectedUser);

        _mockUserManager.Setup(
            userManager => userManager.CheckPasswordAsync(
                expectedUser, validLoginRequest.Password)
        ).ReturnsAsync(false);

        _mockUserManager.Setup(
            userManager => userManager.GetRolesAsync(expectedUser)
        ).ReturnsAsync(
            new List<string> { "Customer" });

        _mockSignInManager.Setup(
            signInManager => signInManager.PasswordSignInAsync(
                It.IsIn<string>(users.Select(u => u.Email)!),
                It.IsNotIn<string>(new List<string> { expectedUser.PasswordHash! }.Concat(users.Select(u => u.PasswordHash))!),
                It.IsAny<bool>(),
                It.IsAny<bool>())
        ).ReturnsAsync(SignInResult.Failed);

        _mockSignInManager.Setup(
            signInManager => signInManager.CreateUserPrincipalAsync(It.IsAny<User>())
        ).ReturnsAsync(new ClaimsPrincipal());

        var authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object
        );

        // Act
        var response = await authService.LoginUser(validLoginRequest.Email, validLoginRequest.Password);

        // Assert
        Assert.False(response.IsSuccess);
        Assert.Equal(StatusCodes.Status401Unauthorized, response.StatusCode);
    }

    [Fact]
    public async Task RegisterUser_ShouldReturnSuccessResponse_WhenUserIsRegisteredSuccessfully()
    {
        // Arrange
        var registrationRequest = new RegistrationRequest(
            email: "unique@example.com",
            firstName: "Test",
            lastName: "User",
            phoneNumber: "123455468",
            password: "password",
            confirmPassword: "password");
        var expectedUser = new User(
                email: registrationRequest.Email,
                firstName: registrationRequest.FirstName,
                lastName: registrationRequest.LastName,
                defaultShippingAddress: registrationRequest.DefaultShippingAddress,
                billingAddress: registrationRequest.BillingAddress);


        _mockUserManager.Setup(
            m => m.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(null as User);

        _mockUserManager.Setup(
            userManager => userManager.AddToRoleAsync(
                It.IsAny<User>(), It.IsAny<string>())
        ).ReturnsAsync(IdentityResult.Success);

        _mockUserManager.Setup(
            userManager => userManager.GetRolesAsync(It.IsAny<User>())
        ).ReturnsAsync(new List<string>(){Roles.Customer});

        _mockUserAccountService.Setup(
            userAccountService => userAccountService.GetUserRole(It.IsAny<User>())
        ).ReturnsAsync(Roles.Customer);

        // Act
        var result = await _authService.RegisterCustomer(registrationRequest.ToUser(), registrationRequest.Password);

        // Assert
        // Assert.True(result.IsSuccess);
        // Assert.NotNull(result.Data);
        Assert.Equal(StatusCodes.Status201Created, result.StatusCode);
    }

    [Fact]
    public async Task RegisterUser_ShouldReturnConflictResponse_WhenUserAlreadyExists()
    {
        // Arrange
        var registrationRequest = new RegistrationRequest(
            email: "test@example.com",
            firstName: "Test",
            lastName: "User",
            phoneNumber: "123455468",
            password: "password",
            confirmPassword: "password");

        _mockUserManager.Setup(
            m => m.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new User(
                email: registrationRequest.Email,
                firstName: registrationRequest.FirstName,
                lastName: registrationRequest.LastName,
                defaultShippingAddress: registrationRequest.DefaultShippingAddress,
                billingAddress: registrationRequest.BillingAddress));

        // Act
        var result = await _authService.RegisterCustomer(registrationRequest.ToUser(), registrationRequest.Password);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal(StatusCodes.Status409Conflict, result.StatusCode);
        Assert.Equal("Email already in use", result.Error.ErrorDescription);
    }

    // refresh
    [Fact]
    public async Task RefreshToken_ShouldReturnNewTokens_OnValidRefreshRequest()
    {
        // Arrange        
        var expectedUser = users[0];
        expectedUser.RefreshToken = _mockJwtTokenGenerator.GenerateRefreshToken();

        var expiredAccessToken = _mockJwtTokenGenerator.GenerateToken(expectedUser, new List<Claim>());

        // setup mocks
        _mockUserManager.Setup(
            userManager => userManager.FindByIdAsync(expectedUser.Id.ToString())
        ).ReturnsAsync(
            expectedUser);

        // _mockUserManager.Setup(
        //     userManager => userManager.UpdateAsync(It.IsAny<User>())
        // );
        _mockUserAccountService.Setup(
            userAccountService => userAccountService.GetUserRole(It.IsAny<User>())
        ).ReturnsAsync(Roles.Customer);

        var authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object);


        // Act
        var result = await authService.RefreshToken(expiredAccessToken, expectedUser.RefreshToken);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal(StatusCodes.Status200OK, result.StatusCode);
    }

    [Fact]
    public async Task RefreshToken_ShouldReturnNotFound_OnInvalidExpiredAccessToken()
    {
        // Arrange        
        var expectedUser = users[0];
        expectedUser.RefreshToken = _mockJwtTokenGenerator.GenerateRefreshToken();

        var expiredAccessToken = _mockJwtTokenGenerator.GenerateToken(expectedUser, new List<Claim>());

        // setup mocks
        _mockUserManager.Setup(
            userManager => userManager.FindByIdAsync(expectedUser.Id.ToString())
        ).ReturnsAsync((User?)null);

        // _mockUserManager.Setup(
        //     userManager => userManager.UpdateAsync(It.IsAny<User>())
        // );
        _mockUserAccountService.Setup(
            userAccountService => userAccountService.GetUserRole(It.IsAny<User>())
        ).ReturnsAsync(Roles.Customer);

        var authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object);


        // Act
        var result = await authService.RefreshToken(expiredAccessToken, expectedUser.RefreshToken);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal(StatusCodes.Status404NotFound, result.StatusCode);
    }

    [Fact]
    public async Task RefreshToken_ShouldReturnUnauthorized_OnInvalidRefreshToken()
    {
        // Arrange        
        var expectedUser = users[0];
        expectedUser.RefreshToken = _mockJwtTokenGenerator.GenerateRefreshToken();
        var invalidRefreshToken = "someinvalidrefreshtoken";

        var expiredAccessToken = _mockJwtTokenGenerator.GenerateToken(expectedUser, new List<Claim>());

        // setup mocks
        _mockUserManager.Setup(
            userManager => userManager.FindByIdAsync(expectedUser.Id.ToString())
        ).ReturnsAsync(expectedUser);

        _mockUserAccountService.Setup(
            userAccountService => userAccountService.GetUserRole(It.IsAny<User>())
        ).ReturnsAsync(Roles.Customer);

        var authService = new AuthService(
            userAccountService: _mockUserAccountService.Object,
            logger: _mockLogger.Object,
            emailService: _mockEmailService.Object,
            tokenGenerator: _mockJwtTokenGenerator,
            userManager: _mockUserManager.Object,
            signInManager: _mockSignInManager.Object);

        // Act
        var result = await authService.RefreshToken(expiredAccessToken, invalidRefreshToken);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal(StatusCodes.Status401Unauthorized, result.StatusCode);
    }
}
