using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using Ecommerce.Controllers.Contracts;
using Ecommerce.Tests.Configuration;
using Ecommerce.Tests.EndToEnd.Configuration;
using Flurl.Http;
using Microsoft.Extensions.DependencyInjection;
using Sentry.Protocol;
using Xunit;
using Xunit.Abstractions;

namespace Ecommerce.Tests.EndToEnd;

public class UserE2ETest : IClassFixture<CustomWebApplicationFactory<Program>>
{
    private readonly CustomWebApplicationFactory<Program> _factory;
    private readonly HttpClient _client;
    private readonly ITestOutputHelper _output;
    private readonly ApiTokenHelper _apiTokenHelper;

    public UserE2ETest(
        CustomWebApplicationFactory<Program> factory,
        ITestOutputHelper output)
    {
        _factory = factory;
        _client = _factory.CreateClient();
        _output = output;

        var scopedServices = factory.Services.CreateScope().ServiceProvider;
        _apiTokenHelper = ApiTokenHelper.FromServiceProviderFactory(scopedServices);
    }


    [Fact]
    public async Task User_OnValidToken_ReturnsUserDetails()
    {
        // Arrange
        var userToken = await _apiTokenHelper.GetCustomerAuthToken();   // Auth token of a customer with email customer@gmail.com
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userToken);

        // Act
        var response = await _client.GetAsync("user");
        var responseContent = await response.Content.ReadAsStringAsync();
        
        // Assert
        response.EnsureSuccessStatusCode();
        Assert.Contains("customer@gmail.com", responseContent);
    }

    [Fact]
    public async Task UserUpdate_OnValidToken_Returns200Success()
    {
        // Arrange
        var userToken = await _apiTokenHelper.GetCustomerAuthToken();
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            "Bearer", userToken);
        var newPhoneNumber = "+2519876543";
        var patchRequest = new UserPatchRequest()
        {
            PhoneNumber = newPhoneNumber
        };

        // Act
        var jsonContents = new StringContent(
            JsonSerializer.Serialize(patchRequest), Encoding.UTF8, "application/json");
        var response = await _client.PatchAsync("user/update", jsonContents);

        // Assert
        response.EnsureSuccessStatusCode();
        var responseContent = await response.Content.ReadAsStringAsync();
        _output.WriteLine(responseContent);
        // Assert.Contains(newPhoneNumber, responseContent);
    }
    
    [Fact]
    public async Task UserPasswordUpdateOnInvalidOldPassword_Returns400BadRequest()
    {
        var userToken = await _apiTokenHelper.GetCustomerAuthToken();
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            "Bearer", userToken);
        var oldPassword = "SomeIncorrectOldPassword";
        var newPassword = "SomePassword";
        var patchRequest = new UserPatchRequest()
        {
            OldPassword = oldPassword,
            NewPassword = newPassword
        };


        var jsonContents = new StringContent(
            JsonSerializer.Serialize(patchRequest), Encoding.UTF8, "application/json");
        var response = await _client.PatchAsync("user/update", jsonContents);

        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    [Fact]
    public async Task UserPasswordUpdateOnMissingOldPassword_Returns400BadRequest()
    {
        var userToken = await _apiTokenHelper.GetCustomerAuthToken();
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            "Bearer", userToken);
        var newPassword = "SomePassword";
        var patchRequest = new UserPatchRequest()
        {
            NewPassword = newPassword
        };

        var jsonContents = new StringContent(
            JsonSerializer.Serialize(patchRequest), Encoding.UTF8, "application/json");
        var response = await _client.PatchAsync("user/update", jsonContents);

        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    // update user password with some wrong password combination

    // delete user
}