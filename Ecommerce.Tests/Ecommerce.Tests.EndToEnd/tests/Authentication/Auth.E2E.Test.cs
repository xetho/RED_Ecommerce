using System.Net;
using System.Text;
using System.Text.Json;
using Ecommerce.Controllers.Contracts;
using Ecommerce.Data;
using Ecommerce.Tests.Configuration;
using Xunit;
using Xunit.Abstractions;

namespace Ecommerce.Tests.EndToEnd;

public class AuthE2ETest : IClassFixture<CustomWebApplicationFactory<Program>>
{
    private readonly HttpClient _client;
    private readonly CustomWebApplicationFactory<Program> _factory;
    private readonly ITestOutputHelper _output;
    private readonly string _BASE_ROUTE = "auth";

    public AuthE2ETest(CustomWebApplicationFactory<Program> factory, ITestOutputHelper output)
    {
        _factory = factory;
        _client = _factory.CreateClient();
        _output = output;

    }

    [Fact]
    public async Task SampleTest()
    {
        var result = await _client.GetAsync($"{_BASE_ROUTE}/test");
        Console.WriteLine(result);
        return;
    }

    [Fact]
    public async Task Register_OnCorrectValues_Returns200Success()
    {
        // Arrange
        // Setting up new details for some new user.
        var email = "someeemail@email.email";
        var password = "some_password";
        var registrationRequest = new RegistrationRequest(
            firstName: email,
            lastName: email,
            email: email,
            password: password,
            confirmPassword: password,
            phoneNumber: "00000000",
            callbackUrl: "www.google.com"
        );

        // Act
        var jsonContent = new StringContent(JsonSerializer.Serialize(registrationRequest), Encoding.UTF8, "application/json");
        var response = await _client.PostAsync("auth/register", jsonContent);
        _output.WriteLine(await response.Content.ReadAsStringAsync());

        // Assert
        response.EnsureSuccessStatusCode();
        _output.WriteLine(response.ToString());
        Assert.True(true);
    }

    [Fact]
    public async Task Register_OnConfirmPasswordMismatch_Returns404BadRequest()
    {
        // Arrange
        // Setting up new details for some new user.
        var email = "someeemail@gmail.email";
        var password = "customerPassword";
        var wrongPasswordConfirm = "some_pass";
        var registrationRequest = new RegistrationRequest(
            firstName: email,
            lastName: email,
            email: email,
            password: password,
            confirmPassword: wrongPasswordConfirm,
            phoneNumber: "00000000",
            callbackUrl: "www.google.com"
        );

        // Act
        var jsonContent = new StringContent(JsonSerializer.Serialize(registrationRequest), Encoding.UTF8, "application/json");
        var response = await _client.PostAsync("auth/register", jsonContent);
        _output.WriteLine(await response.Content.ReadAsStringAsync());

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        _output.WriteLine(response.ToString());
        Assert.True(true);
    }

    [Fact]
    public async Task Register_OnDuplicateEmail_Returns409Conflict()
    {
        // Arrange
        // Setting up new details for some new user. using the same email as a user already in the database.
        var email = "customer@gmail.com";
        var password = "some_password";
        var registrationRequest = new RegistrationRequest(
            firstName: email,
            lastName: email,
            email: email,
            password: password,
            confirmPassword: password,
            phoneNumber: "00000000",
            callbackUrl: "www.google.com"
        );

        // Act
        var jsonContent = new StringContent(JsonSerializer.Serialize(registrationRequest), Encoding.UTF8, "application/json");
        var response = await _client.PostAsync($"{_BASE_ROUTE}/register", jsonContent);
        _output.WriteLine(await response.Content.ReadAsStringAsync());

        // Assert
        Assert.Equal(HttpStatusCode.Conflict, response.StatusCode);
        _output.WriteLine(response.ToString());
        Assert.True(true);
    }

    [Fact]
    public async Task Login_OnCorrectCredentials_Returns200Success()
    {
        // Arrange
        // Attempt login with a user created during the db initialization(seeding).
        // (tests run in parallel, we can't exactly login with the user from the test above)
        // another way would be to register first then login as that user, but that's coupling.
        var email = "admin@gmail.com";
        var password = "adminPassword";
        var loginRequest = new LoginRequest(email, password);
        var jsonContent = new StringContent(JsonSerializer.Serialize(loginRequest), Encoding.UTF8, "application/json");

        var response = await _client.PostAsync($"{_BASE_ROUTE}/login", jsonContent);
        response.EnsureSuccessStatusCode();

        var stringResponse = await response.Content.ReadAsStringAsync();
        // var model = stringResponse.FromJson<LoginRequest>();

        Assert.True(true);
    }

    [Fact]
    public async Task Login_OnIncorrectPassword_Returns401UnauthenticatedResponse()
    {
        // Arrange
        // Login with the same user as above but with the wrong password
        var email = "admin@gmail.com";
        var password = "theAbsoluteWrongestPassword";
        var loginRequest = new LoginRequest(email, password);
        var jsonContent = new StringContent(JsonSerializer.Serialize(loginRequest), Encoding.UTF8, "application/json");

        var response = await _client.PostAsync($"{_BASE_ROUTE}/login", jsonContent);
        _output.WriteLine(response.StatusCode.ToString());
        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
    }

    [Fact]
    public async Task Login_OnIncorrectEmail_Returns404NotFoundResponse()
    {
        // Arrange
        // Login with the same user as above but with the wrong password
        var email = "theSecondWrongestEmail@email.email";
        var password = "adminpassword";
        var loginRequest = new LoginRequest(email, password);
        var jsonContent = new StringContent(JsonSerializer.Serialize(loginRequest), Encoding.UTF8, "application/json");

        var response = await _client.PostAsync($"{_BASE_ROUTE}/login", jsonContent);
        _output.WriteLine(response.StatusCode.ToString());
        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }

    // [Fact]
    // public async Task EmailConfirm_OnCorrectToken_Returns200Ok()
    // {
    //     // will need to register, then request the token, and then confirm. bit too many steps but still. Would also need more since the token is only sent to the email. welp
    // }
}