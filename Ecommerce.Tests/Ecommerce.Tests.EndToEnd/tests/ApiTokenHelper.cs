using System.Security.Claims;
using Ecommerce.Models;
using Ecommerce.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Ecommerce.Tests.EndToEnd.Configuration;

public class ApiTokenHelper
{
    // Can't just make a token, userID depends on what's saved in the db too. To make this accurate 
    // should get the users with userManager and return an actual token instead.
    // Since the token generator itself isn't dependent on authentication logic, and just a user entity...
    private readonly IJwtTokenGenerator _jwtTokenGenerator;
    private readonly UserManager<User> _userManager;
    private readonly SignInManager<User> _signInManager;
    public ApiTokenHelper(
        IJwtTokenGenerator tokenGenerator,
        SignInManager<User> signInManager,
        UserManager<User> userManager)
    {
        _jwtTokenGenerator = tokenGenerator;
        _userManager = userManager;
        _signInManager = signInManager;
    }

    /// <summary>
    /// Creates a new ApiTokenHelper using the required services resolved from the IServiceProvider.
    /// </summary>
    /// <param name="serviceProvider"></param>
    /// <returns></returns>
    public static ApiTokenHelper FromServiceProviderFactory(IServiceProvider serviceProvider)
    {
        // Builds the token helper from the scoped service provider.
        var tokenGenerator = serviceProvider.GetRequiredService<IJwtTokenGenerator>() ?? 
            throw new Exception(@"IJwtTokenGenerator not found. Make sure the provided IServiceProvider 
                has an instance of IJwtTokenGenerator registered to it.");

        var userManager = serviceProvider.GetRequiredService<UserManager<User>>()  ??
            throw new Exception(@"UserManager not found. Make sure the provided IServiceProvider 
                has an instance of UserManager<TUser> registered to it.");

        var signInManager = serviceProvider.GetRequiredService<SignInManager<User>>() ?? 
            throw new Exception(@"SignInManager not found. Make sure the provided IServiceProvider 
                has an instance of SignInManager<TUser> registered to it.");

        return new ApiTokenHelper(tokenGenerator, signInManager, userManager);
    }

    /// <summary>
    /// Returns the access token of an admin user(email: "admin@gmail.com").
    /// </summary>
    /// <returns></returns>
    public async Task<string> GetAdminAuthToken()
    {
        string email = "admin@gmail.com";
        var admin = await _userManager.FindByEmailAsync(email);
        var principal = await _signInManager.CreateUserPrincipalAsync(admin);
        var claims = principal.Claims.ToList();

        return _jwtTokenGenerator.GenerateToken(admin, claims);
    }

    /// <summary>
    /// Returns the access token of a customer user(email: "customer@gmail.com").
    /// </summary>
    /// <returns></returns>
    public async Task<string> GetCustomerAuthToken()
    {
        string email = "customer@gmail.com";
        var customer = await _userManager.FindByEmailAsync(email);
        var principal = await _signInManager.CreateUserPrincipalAsync(customer);
        var claims = principal.Claims.ToList();

        return _jwtTokenGenerator.GenerateToken(customer, claims);
    }
}