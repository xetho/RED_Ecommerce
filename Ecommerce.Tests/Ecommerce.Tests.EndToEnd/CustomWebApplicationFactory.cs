using Ecommerce.Configuration;
using Ecommerce.Data;
using Ecommerce.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Ecommerce.Tests.EndToEnd;

/// <summary>
/// Custom WebAppFactory for end-to-end testing.
/// </summary>
/// <remarks>
/// Runs a new instance of Program.cs(the web-api itself) with custom test specific configuration
/// such as using an InMemory Database or changing env variables to test values.
/// </remarks>
public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
{
    private readonly SqliteConnection _connection;
    
    public CustomWebApplicationFactory()
    {
        // using an in-memory connection with sqlite. 
        _connection = new SqliteConnection("DataSource=:memory:");
        _connection.Open();
    }

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        var TEST_CONFIGURATION_PATH = "appsettings.test.json";

        string jsonFilePath = Path.Combine(AppContext.BaseDirectory, TEST_CONFIGURATION_PATH);

        // Ensure configuration file exists.
        if (!File.Exists(jsonFilePath)) throw new FileNotFoundException(
            @"Please make sure the file appsettings.test.json exists in the base directory of the project,
                and has the required configuration values.");

        // Build the Configuration using the json file (should be in the root of the main project).
        var configuration = new ConfigurationBuilder()
            .AddJsonFile(TEST_CONFIGURATION_PATH)
            .Build();

        builder.UseConfiguration(configuration).ConfigureAppConfiguration(
                (context, configBuilder) => configBuilder.AddJsonFile(TEST_CONFIGURATION_PATH));

        builder.ConfigureLogging(config => config.AddConsole());

        builder.ConfigureServices(services =>
        {
            services.AddLogging(builder => builder.AddConsole());

            // Replace Original Database connection (And DbContext) with a new one for testing.
            ServiceDescriptor? existingDbConnection = services.SingleOrDefault(
                serviceDescriptor => serviceDescriptor.ServiceType == typeof(DbContextOptions<ApplicationDbContext>));

            // Remove the existing connection if it exists.
            if (existingDbConnection != null) services.Remove(existingDbConnection);

            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlite(
                    _connection));    

            var serviceProvider = services.BuildServiceProvider();
        });

        // Seed the database and define the roles. Yes role creation is also done later on in program.cs but we can't
        // seed users without doing it here now.
        builder.ConfigureServices(async services => 
        {
            var serviceProvider = services.BuildServiceProvider();
            using(var scope = serviceProvider.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<ApplicationDbContext>();
                db.Database.EnsureCreated();

                // Add roles.
                var roles = Roles.GetRoles();
                await scopedServices.AddRoles(roles);
                await scopedServices.InitializeDbForTest();
            }
        });
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        _connection.Dispose();
    }
}