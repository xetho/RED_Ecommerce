using Ecommerce.Data;
using Ecommerce.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

public static class DependencyInjection
{
    public static async Task<IServiceProvider> InitializeDbForTest(this IServiceProvider serviceProvider)
    {
        // Creating some data specifically for the test.
        var context = serviceProvider.GetRequiredService<ApplicationDbContext>();

        var userManager = serviceProvider.GetRequiredService<UserManager<User>>();

        // Create a test customer and a test admin.
        var customer = new User(
            "customer@gmail.com",
            "Test Customer",
            "The Customer",
            "John Cena House",
            "You can't see it anyway");
        var customerPassword = "customerPassword";

        var customer2 = new User(
            "customer2@gmail.com",
            "Test Customer",
            "The Customer",
            "John Cena House",
            "You can't see it anyway");
        var customer2Password = "customer2Password";

        var admin = new User(
            "admin@gmail.com",
            "Test Admin",
            "The Admin",
            "The Rock House",
            "A rock, obviously are you dumb");
        var adminPassword = "adminPassword";

        await userManager.CreateAsync(customer, customerPassword);
        await userManager.CreateAsync(customer2, customer2Password);
        await userManager.CreateAsync(admin, adminPassword);

        await userManager.AddToRoleAsync(customer, Roles.Customer);
        await userManager.AddToRoleAsync(customer2, Roles.Customer);
        await userManager.AddToRoleAsync(admin, Roles.Admin);

        // Leaving the second customer's email unconfirmed in case I want to test that functionality later.
        if (!customer.EmailConfirmed)
        {
            var customerConfirmToken = await userManager.GenerateEmailConfirmationTokenAsync(customer);
            await userManager.ConfirmEmailAsync(customer, customerConfirmToken);
        }
        if (!admin.EmailConfirmed)
        {
            var adminConfirmToken = await userManager.GenerateEmailConfirmationTokenAsync(admin);
            await userManager.ConfirmEmailAsync(admin, adminConfirmToken);
        }


        // Add anything else you wish to test here...

        return serviceProvider;
    }
}